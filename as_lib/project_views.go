package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// View represents a Semaphore view.
type View struct {
    ID        int    `json:"id"`
    Title     string `json:"title"`
    ProjectID int    `json:"project_id"`
    Position  int    `json:"position"`
}

// GetViews retrieves views for a project by project ID using a GET request to the Semaphore API.
func GetViews(client *SemaphoreClient, projectID int) ([]View, error) {
    // Define the API endpoint URL for getting views for a project
    getViewsURL := fmt.Sprintf("%s/project/%d/views", client.baseURL, projectID)

    // Create an HTTP GET request for getting views
    req, err := client.createRequest("GET", getViewsURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get views for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var views []View
    if err := json.NewDecoder(resp.Body).Decode(&views); err != nil {
        return nil, err
    }

    return views, nil
}

// CreateView creates a view for a project by project ID using a POST request to the Semaphore API.
func CreateView(client *SemaphoreClient, projectID int, newView View) error {
    // Define the API endpoint URL for creating a view for a project
    createViewURL := fmt.Sprintf("%s/project/%d/views", client.baseURL, projectID)

    // Serialize the newView to JSON
    requestData, err := json.Marshal(newView)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for creating the view
    req, err := client.createRequest("POST", createViewURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to create view, status code: %d", resp.StatusCode)
    }

    // The view has been successfully created

    return nil
}

// GetView retrieves a view by project ID and view ID using a GET request to the Semaphore API.
func GetView(client *SemaphoreClient, projectID, viewID int) (*View, error) {
    // Define the API endpoint URL for getting a view by project ID and view ID
    getViewURL := fmt.Sprintf("%s/project/%d/views/%d", client.baseURL, projectID, viewID)

    // Create an HTTP GET request for getting the view
    req, err := client.createRequest("GET", getViewURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get view, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var view View
    if err := json.NewDecoder(resp.Body).Decode(&view); err != nil {
        return nil, err
    }

    return &view, nil
}

// UpdateView updates a view by project ID and view ID using a PUT request to the Semaphore API.
func UpdateView(client *SemaphoreClient, projectID, viewID int, updatedView View) error {
    // Define the API endpoint URL for updating a view by project ID and view ID
    updateViewURL := fmt.Sprintf("%s/project/%d/views/%d", client.baseURL, projectID, viewID)

    // Serialize the updatedView to JSON
    requestData, err := json.Marshal(updatedView)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the view
    req, err := client.createRequest("PUT", updateViewURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update view, status code: %d", resp.StatusCode)
    }

    // The view has been successfully updated

    return nil
}

// RemoveView removes a view by project ID and view ID using a DELETE request to the Semaphore API.
func RemoveView(client *SemaphoreClient, projectID, viewID int) error {
    // Define the API endpoint URL for removing a view by project ID and view ID
    removeViewURL := fmt.Sprintf("%s/project/%d/views/%d", client.baseURL, projectID, viewID)

    // Create an HTTP DELETE request for removing the view
    req, err := client.createRequest("DELETE", removeViewURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove view, status code: %d", resp.StatusCode)
    }

    // The view has been successfully removed

    return nil
}
