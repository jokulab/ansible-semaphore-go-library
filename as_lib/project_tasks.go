package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Task represents a Semaphore task.
type Task struct {
    ID          int    `json:"id"`
    TemplateID  int    `json:"template_id"`
    Status      string `json:"status"`
    Debug       bool   `json:"debug"`
    Playbook    string `json:"playbook"`
    Environment string `json:"environment"`
    Limit       string `json:"limit"`
}

// GetTasks retrieves tasks related to a project by project ID using a GET request to the Semaphore API.
func GetTasks(client *SemaphoreClient, projectID int) ([]Task, error) {
    // Define the API endpoint URL for getting tasks related to a project
    getTasksURL := fmt.Sprintf("%s/project/%d/tasks", client.baseURL, projectID)

    // Create an HTTP GET request for getting tasks
    req, err := client.createRequest("GET", getTasksURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get tasks for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var tasks []Task
    if err := json.NewDecoder(resp.Body).Decode(&tasks); err != nil {
        return nil, err
    }

    return tasks, nil
}

// StartTask starts a job by creating a task for a project by project ID using a POST request to the Semaphore API.
func StartTask(client *SemaphoreClient, projectID int, newTask Task) (*Task, error) {
    // Define the API endpoint URL for starting a task for a project
    startTaskURL := fmt.Sprintf("%s/project/%d/tasks", client.baseURL, projectID)

    // Serialize the newTask to JSON
    requestData, err := json.Marshal(newTask)
    if err != nil {
        return nil, err
    }

    // Create an HTTP POST request for starting the task
    req, err := client.createRequest("POST", startTaskURL, requestData)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to start task, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var task Task
    if err := json.NewDecoder(resp.Body).Decode(&task); err != nil {
        return nil, err
    }

    return &task, nil
}

// GetLastTasks retrieves the last 200 tasks related to a project by project ID using a GET request to the Semaphore API.
func GetLastTasks(client *SemaphoreClient, projectID int) ([]Task, error) {
    // Define the API endpoint URL for getting the last 200 tasks related to a project
    getLastTasksURL := fmt.Sprintf("%s/project/%d/tasks/last", client.baseURL, projectID)

    // Create an HTTP GET request for getting the last tasks
    req, err := client.createRequest("GET", getLastTasksURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get last tasks for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var tasks []Task
    if err := json.NewDecoder(resp.Body).Decode(&tasks); err != nil {
        return nil, err
    }

    return tasks, nil
}

// StopTask stops a job by task ID for a project by project ID using a POST request to the Semaphore API.
func StopTask(client *SemaphoreClient, projectID, taskID int) error {
    // Define the API endpoint URL for stopping a task by project ID and task ID
    stopTaskURL := fmt.Sprintf("%s/project/%d/tasks/%d/stop", client.baseURL, projectID, taskID)

    // Create an HTTP POST request for stopping the task
    req, err := client.createRequest("POST", stopTaskURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to stop task, status code: %d", resp.StatusCode)
    }

    return nil
}

// GetTask retrieves a single task by task ID for a project by project ID using a GET request to the Semaphore API.
func GetTask(client *SemaphoreClient, projectID, taskID int) (*Task, error) {
    // Define the API endpoint URL for getting a single task by project ID and task ID
    getTaskURL := fmt.Sprintf("%s/project/%d/tasks/%d", client.baseURL, projectID, taskID)

    // Create an HTTP GET request for getting the task
    req, err := client.createRequest("GET", getTaskURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get task, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var task Task
    if err := json.NewDecoder(resp.Body).Decode(&task); err != nil {
        return nil, err
    }

    return &task, nil
}
