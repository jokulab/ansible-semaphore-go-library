package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Template represents a Semaphore template.
type Template struct {
    ID                    int       `json:"id"`
    ProjectID             int       `json:"project_id"`
    InventoryID           int       `json:"inventory_id"`
    RepositoryID          int       `json:"repository_id"`
    EnvironmentID         int       `json:"environment_id"`
    ViewID                int       `json:"view_id"`
    Name                  string    `json:"name"`
    Playbook              string    `json:"playbook"`
    Arguments             string    `json:"arguments"`
    Description           string    `json:"description"`
    AllowOverrideArgs     bool      `json:"allow_override_args_in_task"`
    SuppressSuccessAlerts bool      `json:"suppress_success_alerts"`
    SurveyVars            []SurveyVar `json:"survey_vars"`
}

// SurveyVar represents a Semaphore survey variable.
type SurveyVar struct {
    Name        string `json:"name"`
    Title       string `json:"title"`
    Description string `json:"description"`
    Type        string `json:"type"`
    Required    bool   `json:"required"`
}

// GetTemplates retrieves templates for a project by project ID using a GET request to the Semaphore API.
func GetTemplates(client *SemaphoreClient, projectID int, sort, order string) ([]Template, error) {
    // Define the API endpoint URL for getting templates for a project
    getTemplatesURL := fmt.Sprintf("%s/project/%d/templates?sort=%s&order=%s", client.baseURL, projectID, sort, order)

    // Create an HTTP GET request for getting templates
    req, err := client.createRequest("GET", getTemplatesURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get templates for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var templates []Template
    if err := json.NewDecoder(resp.Body).Decode(&templates); err != nil {
        return nil, err
    }

    return templates, nil
}

// CreateTemplate creates a template for a project by project ID using a POST request to the Semaphore API.
func CreateTemplate(client *SemaphoreClient, projectID int, newTemplate Template) error {
    // Define the API endpoint URL for creating a template for a project
    createTemplateURL := fmt.Sprintf("%s/project/%d/templates", client.baseURL, projectID)

    // Serialize the newTemplate to JSON
    requestData, err := json.Marshal(newTemplate)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for creating the template
    req, err := client.createRequest("POST", createTemplateURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to create template, status code: %d", resp.StatusCode)
    }

    // The template has been successfully created

    return nil
}

// GetTemplate retrieves a template by project ID and template ID using a GET request to the Semaphore API.
func GetTemplate(client *SemaphoreClient, projectID, templateID int) (*Template, error) {
    // Define the API endpoint URL for getting a template by project ID and template ID
    getTemplateURL := fmt.Sprintf("%s/project/%d/templates/%d", client.baseURL, projectID, templateID)

    // Create an HTTP GET request for getting the template
    req, err := client.createRequest("GET", getTemplateURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get template, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var template Template
    if err := json.NewDecoder(resp.Body).Decode(&template); err != nil {
        return nil, err
    }

    return &template, nil
}

// UpdateTemplate updates a template by project ID and template ID using a PUT request to the Semaphore API.
func UpdateTemplate(client *SemaphoreClient, projectID, templateID int, updatedTemplate Template) error {
    // Define the API endpoint URL for updating a template by project ID and template ID
    updateTemplateURL := fmt.Sprintf("%s/project/%d/templates/%d", client.baseURL, projectID, templateID)

    // Serialize the updatedTemplate to JSON
    requestData, err := json.Marshal(updatedTemplate)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the template
    req, err := client.createRequest("PUT", updateTemplateURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update template, status code: %d", resp.StatusCode)
    }

    // The template has been successfully updated

    return nil
}

// RemoveTemplate removes a template by project ID and template ID using a DELETE request to the Semaphore API.
func RemoveTemplate(client *SemaphoreClient, projectID, templateID int) error {
    // Define the API endpoint URL for removing a template by project ID and template ID
    removeTemplateURL := fmt.Sprintf("%s/project/%d/templates/%d", client.baseURL, projectID, templateID)

    // Create an HTTP DELETE request for removing the template
    req, err := client.createRequest("DELETE", removeTemplateURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove template, status code: %d", resp.StatusCode)
    }

    // The template has been successfully removed

    return nil
}
