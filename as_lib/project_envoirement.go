package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Environment represents a Semaphore environment.
type Environment struct {
    ID         int    `json:"id"`
    Name       string `json:"name"`
    ProjectID  int    `json:"project_id"`
    Password   string `json:"password"`
    JSON       string `json:"json"`
    Env        string `json:"env"`
}

// GetEnvironment retrieves environment for a project by project ID using a GET request to the Semaphore API.
func GetEnvironment(client *SemaphoreClient, projectID int, sort, order string) ([]Environment, error) {
    // Define the API endpoint URL for getting environment for a project
    getEnvironmentURL := fmt.Sprintf("%s/project/%d/environment?sort=%s&order=%s", client.baseURL, projectID, sort, order)

    // Create an HTTP GET request for getting environment
    req, err := client.createRequest("GET", getEnvironmentURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get environment for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var environment []Environment
    if err := json.NewDecoder(resp.Body).Decode(&environment); err != nil {
        return nil, err
    }

    return environment, nil
}

// AddEnvironment adds environment to a project by project ID using a POST request to the Semaphore API.
func AddEnvironment(client *SemaphoreClient, projectID int, newEnvironment Environment) error {
    // Define the API endpoint URL for adding environment to a project
    addEnvironmentURL := fmt.Sprintf("%s/project/%d/environment", client.baseURL, projectID)

    // Serialize the newEnvironment to JSON
    requestData, err := json.Marshal(newEnvironment)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for adding the environment
    req, err := client.createRequest("POST", addEnvironmentURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to add environment, status code: %d", resp.StatusCode)
    }

    // The environment has been successfully added to the project

    return nil
}

// UpdateEnvironment updates environment by project ID and environment ID using a PUT request to the Semaphore API.
func UpdateEnvironment(client *SemaphoreClient, projectID, environmentID int, updatedEnvironment Environment) error {
    // Define the API endpoint URL for updating environment by project ID and environment ID
    updateEnvironmentURL := fmt.Sprintf("%s/project/%d/environment/%d", client.baseURL, projectID, environmentID)

    // Serialize the updatedEnvironment to JSON
    requestData, err := json.Marshal(updatedEnvironment)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the environment
    req, err := client.createRequest("PUT", updateEnvironmentURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update environment, status code: %d", resp.StatusCode)
    }

    // The environment has been successfully updated

    return nil
}

// RemoveEnvironment removes environment by project ID and environment ID using a DELETE request to the Semaphore API.
func RemoveEnvironment(client *SemaphoreClient, projectID, environmentID int) error {
    // Define the API endpoint URL for removing environment by project ID and environment ID
    removeEnvironmentURL := fmt.Sprintf("%s/project/%d/environment/%d", client.baseURL, projectID, environmentID)

    // Create an HTTP DELETE request for removing the environment
    req, err := client.createRequest("DELETE", removeEnvironmentURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove environment, status code: %d", resp.StatusCode)
    }

    // The environment has been successfully removed

    return nil
}
