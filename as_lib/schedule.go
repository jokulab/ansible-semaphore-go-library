package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Schedule represents a Semaphore schedule.
type Schedule struct {
    ID          int    `json:"id"`
    CronFormat  string `json:"cron_format"`
    ProjectID   int    `json:"project_id"`
    TemplateID  int    `json:"template_id"`
}

// GetSchedule retrieves a schedule by project ID and schedule ID using a GET request to the Semaphore API.
func GetSchedule(client *SemaphoreClient, projectID, scheduleID int) (*Schedule, error) {
    // Define the API endpoint URL for fetching a schedule by project ID and schedule ID
    getScheduleURL := fmt.Sprintf("%s/project/%d/schedules/%d", client.baseURL, projectID, scheduleID)

    // Create an HTTP GET request for fetching the schedule
    req, err := client.createRequest("GET", getScheduleURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get schedule, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var schedule Schedule
    if err := json.NewDecoder(resp.Body).Decode(&schedule); err != nil {
        return nil, err
    }

    return &schedule, nil
}

// DeleteSchedule deletes a schedule by project ID and schedule ID using a DELETE request to the Semaphore API.
func DeleteSchedule(client *SemaphoreClient, projectID, scheduleID int) error {
    // Define the API endpoint URL for deleting a schedule by project ID and schedule ID
    deleteScheduleURL := fmt.Sprintf("%s/project/%d/schedules/%d", client.baseURL, projectID, scheduleID)

    // Create an HTTP DELETE request for deleting the schedule
    req, err := client.createRequest("DELETE", deleteScheduleURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to delete schedule, status code: %d", resp.StatusCode)
    }

    // The schedule has been successfully deleted

    return nil
}

// UpdateSchedule updates a schedule by project ID and schedule ID using a PUT request to the Semaphore API.
func UpdateSchedule(client *SemaphoreClient, projectID, scheduleID int, updatedSchedule Schedule) error {
    // Define the API endpoint URL for updating a schedule by project ID and schedule ID
    updateScheduleURL := fmt.Sprintf("%s/project/%d/schedules/%d", client.baseURL, projectID, scheduleID)

    // Serialize the updatedSchedule to JSON
    requestData, err := json.Marshal(updatedSchedule)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the schedule
    req, err := client.createRequest("PUT", updateScheduleURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update schedule, status code: %d", resp.StatusCode)
    }

    // The schedule has been successfully updated

    return nil
}

// CreateSchedule creates a new schedule by project ID using a POST request to the Semaphore API.
func CreateSchedule(client *SemaphoreClient, projectID int, newSchedule Schedule) (*Schedule, error) {
    // Define the API endpoint URL for creating a schedule by project ID
    createScheduleURL := fmt.Sprintf("%s/project/%d/schedules", client.baseURL, projectID)

    // Serialize the newSchedule to JSON
    requestData, err := json.Marshal(newSchedule)
    if err != nil {
        return nil, err
    }

    // Create an HTTP POST request for creating the schedule
    req, err := client.createRequest("POST", createScheduleURL, requestData)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to create schedule, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var createdSchedule Schedule
    if err := json.NewDecoder(resp.Body).Decode(&createdSchedule); err != nil {
        return nil, err
    }

    return &createdSchedule, nil
}
