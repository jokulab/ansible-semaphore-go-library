package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Inventory represents a Semaphore inventory.
type Inventory struct {
    ID           int    `json:"id"`
    Name         string `json:"name"`
    ProjectID    int    `json:"project_id"`
    Inventory    string `json:"inventory"`
    SSHKeyID     int    `json:"ssh_key_id"`
    BecomeKeyID  int    `json:"become_key_id"`
    Type         string `json:"type"`
}

// GetInventory retrieves inventory for a project by project ID using a GET request to the Semaphore API.
func GetInventory(client *SemaphoreClient, projectID int, sort, order string) ([]Inventory, error) {
    // Define the API endpoint URL for getting inventory for a project
    getInventoryURL := fmt.Sprintf("%s/project/%d/inventory?sort=%s&order=%s", client.baseURL, projectID, sort, order)

    // Create an HTTP GET request for getting inventory
    req, err := client.createRequest("GET", getInventoryURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get inventory for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var inventory []Inventory
    if err := json.NewDecoder(resp.Body).Decode(&inventory); err != nil {
        return nil, err
    }

    return inventory, nil
}

// CreateInventory creates inventory for a project by project ID using a POST request to the Semaphore API.
func CreateInventory(client *SemaphoreClient, projectID int, newInventory Inventory) (int, error) {
    // Define the API endpoint URL for creating inventory for a project
    createInventoryURL := fmt.Sprintf("%s/project/%d/inventory", client.baseURL, projectID)

    // Serialize the newInventory to JSON
    requestData, err := json.Marshal(newInventory)
    if err != nil {
        return 0, err
    }

    // Create an HTTP POST request for creating the inventory
    req, err := client.createRequest("POST", createInventoryURL, requestData)
    if err != nil {
        return 0, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return 0, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return 0, fmt.Errorf("failed to create inventory, status code: %d", resp.StatusCode)
    }

    // Extract the ID of the created inventory from the response headers
    inventoryIDHeader := resp.Header.Get("Location")
    if inventoryIDHeader == "" {
        return 0, fmt.Errorf("failed to extract created inventory ID from response headers")
    }

    // Parse the inventory ID as an integer
    var inventoryID int
    _, err = fmt.Sscanf(inventoryIDHeader, "%d", &inventoryID)
    if err != nil {
        return 0, err
    }

    // The inventory has been successfully created, and its ID is returned
    return inventoryID, nil
}

// UpdateInventory updates inventory by project ID and inventory ID using a PUT request to the Semaphore API.
func UpdateInventory(client *SemaphoreClient, projectID, inventoryID int, updatedInventory Inventory) error {
    // Define the API endpoint URL for updating inventory by project ID and inventory ID
    updateInventoryURL := fmt.Sprintf("%s/project/%d/inventory/%d", client.baseURL, projectID, inventoryID)

    // Serialize the updatedInventory to JSON
    requestData, err := json.Marshal(updatedInventory)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the inventory
    req, err := client.createRequest("PUT", updateInventoryURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update inventory, status code: %d", resp.StatusCode)
    }

    // The inventory has been successfully updated

    return nil
}

// RemoveInventory removes inventory by project ID and inventory ID using a DELETE request to the Semaphore API.
func RemoveInventory(client *SemaphoreClient, projectID, inventoryID int) error {
    // Define the API endpoint URL for removing inventory by project ID and inventory ID
    removeInventoryURL := fmt.Sprintf("%s/project/%d/inventory/%d", client.baseURL, projectID, inventoryID)

    // Create an HTTP DELETE request for removing the inventory
    req, err := client.createRequest("DELETE", removeInventoryURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove inventory, status code: %d", resp.StatusCode)
    }

    // The inventory has been successfully removed

    return nil
}
