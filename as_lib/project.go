package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// FetchProject fetches a project by project ID using a GET request to the Semaphore API.
func FetchProject(client *SemaphoreClient, projectID int) (*Project, error) {
    // Define the API endpoint URL for fetching a project by ID
    fetchProjectURL := fmt.Sprintf("%s/project/%d", client.baseURL, projectID)

    // Create an HTTP GET request for fetching the project
    req, err := client.createRequest("GET", fetchProjectURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to fetch project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var project Project
    if err := json.NewDecoder(resp.Body).Decode(&project); err != nil {
        return nil, err
    }

    return &project, nil
}

// UpdateProject updates a project by project ID using a PUT request to the Semaphore API.
func UpdateProject(client *SemaphoreClient, projectID int, updatedProject Project) error {
    // Define the API endpoint URL for updating a project by ID
    updateProjectURL := fmt.Sprintf("%s/project/%d", client.baseURL, projectID)

    // Serialize the updatedProject to JSON
    requestData, err := json.Marshal(updatedProject)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the project
    req, err := client.createRequest("PUT", updateProjectURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update project, status code: %d", resp.StatusCode)
    }

    // The project has been successfully updated

    return nil
}

// DeleteProject deletes a project by project ID using a DELETE request to the Semaphore API.
func DeleteProject(client *SemaphoreClient, projectID int) error {
    // Define the API endpoint URL for deleting a project by ID
    deleteProjectURL := fmt.Sprintf("%s/project/%d", client.baseURL, projectID)

    // Create an HTTP DELETE request for deleting the project
    req, err := client.createRequest("DELETE", deleteProjectURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to delete project, status code: %d", resp.StatusCode)
    }

    // The project has been successfully deleted

    return nil
}
