package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
)

// Project represents a Semaphore project.
type Project struct {
    ID               int    `json:"id"`
    Name             string `json:"name"`
    Created          string `json:"created"`
    Alert            bool   `json:"alert"`
    AlertChat        string `json:"alert_chat"`
    MaxParallelTasks int    `json:"max_parallel_tasks"`
}

// GetProjects retrieves a list of projects by making a GET request to the Semaphore API.
func GetProjects(client *SemaphoreClient) ([]Project, error) {
    // Define the API endpoint URL for retrieving projects
    getProjectsURL := fmt.Sprintf("%s/projects", client.baseURL)

    // Create an HTTP GET request for retrieving projects
    req, err := client.createRequest("GET", getProjectsURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get projects, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }

    // Unmarshal the JSON response into a list of projects
    var projects []Project
    if err := json.Unmarshal(body, &projects); err != nil {
        return nil, err
    }

    return projects, nil
}

// CreateProject creates a new project by making a POST request to the Semaphore API.
func CreateProject(client *SemaphoreClient, projectData Project) error {
    // Define the API endpoint URL for creating a project
    createProjectURL := fmt.Sprintf("%s/projects", client.baseURL)

    // Serialize the projectData to JSON
    requestData, err := json.Marshal(projectData)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for creating a project
    req, err := client.createRequest("POST", createProjectURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusCreated {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to create project, status code: %d", resp.StatusCode)
    }

    // Handle the response data if needed

    return nil
}
