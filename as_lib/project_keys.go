package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// AccessKey represents a Semaphore access key.
type AccessKey struct {
    ID            int    `json:"id"`
    Name          string `json:"name"`
    Type          string `json:"type"`
    ProjectID     int    `json:"project_id"`
    LoginPassword struct {
        Password string `json:"password"`
        Login    string `json:"login"`
    } `json:"login_password"`
    SSH struct {
        Login       string `json:"login"`
        PrivateKey  string `json:"private_key"`
    } `json:"ssh"`
}

// GetAccessKeysLinkedToProject retrieves access keys linked to a project by project ID using a GET request to the Semaphore API.
func GetAccessKeysLinkedToProject(client *SemaphoreClient, projectID int, keyType, sort, order string) ([]AccessKey, error) {
    // Define the API endpoint URL for getting access keys linked to a project
    getAccessKeysURL := fmt.Sprintf("%s/project/%d/keys?key_type=%s&sort=%s&order=%s", client.baseURL, projectID, keyType, sort, order)

    // Create an HTTP GET request for getting access keys
    req, err := client.createRequest("GET", getAccessKeysURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get access keys linked to project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var accessKeys []AccessKey
    if err := json.NewDecoder(resp.Body).Decode(&accessKeys); err != nil {
        return nil, err
    }

    return accessKeys, nil
}

// AddAccessKey adds an access key to a project by project ID using a POST request to the Semaphore API.
func AddAccessKey(client *SemaphoreClient, projectID int, newAccessKey AccessKey) error {
    // Define the API endpoint URL for adding an access key to a project
    addAccessKeyURL := fmt.Sprintf("%s/project/%d/keys", client.baseURL, projectID)

    // Serialize the newAccessKey to JSON
    requestData, err := json.Marshal(newAccessKey)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for adding the access key
    req, err := client.createRequest("POST", addAccessKeyURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to add access key, status code: %d", resp.StatusCode)
    }

    // The access key has been successfully added to the project

    return nil
}

// UpdateAccessKey updates an access key by project ID and key ID using a PUT request to the Semaphore API.
func UpdateAccessKey(client *SemaphoreClient, projectID, keyID int, updatedAccessKey AccessKey) error {
    // Define the API endpoint URL for updating an access key by project ID and key ID
    updateAccessKeyURL := fmt.Sprintf("%s/project/%d/keys/%d", client.baseURL, projectID, keyID)

    // Serialize the updatedAccessKey to JSON
    requestData, err := json.Marshal(updatedAccessKey)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the access key
    req, err := client.createRequest("PUT", updateAccessKeyURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update access key, status code: %d", resp.StatusCode)
    }

    // The access key has been successfully updated

    return nil
}

// RemoveAccessKey removes an access key by project ID and key ID using a DELETE request to the Semaphore API.
func RemoveAccessKey(client *SemaphoreClient, projectID, keyID int) error {
    // Define the API endpoint URL for removing an access key by project ID and key ID
    removeAccessKeyURL := fmt.Sprintf("%s/project/%d/keys/%d", client.baseURL, projectID, keyID)

    // Create an HTTP DELETE request for removing the access key
    req, err := client.createRequest("DELETE", removeAccessKeyURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove access key, status code: %d", resp.StatusCode)
    }

    // The access key has been successfully removed

    return nil
}
