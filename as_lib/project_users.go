package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// User represents a Semaphore user.
type User struct {
    ID       int    `json:"id"`
    Name     string `json:"name"`
    Username string `json:"username"`
}

// GetUsersLinkedToProject retrieves users linked to a project by project ID using a GET request to the Semaphore API.
func GetUsersLinkedToProject(client *SemaphoreClient, projectID int, sort, order string) ([]User, error) {
    // Define the API endpoint URL for getting users linked to a project
    getUsersURL := fmt.Sprintf("%s/project/%d/users?sort=%s&order=%s", client.baseURL, projectID, sort, order)

    // Create an HTTP GET request for getting users
    req, err := client.createRequest("GET", getUsersURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get users linked to project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var users []User
    if err := json.NewDecoder(resp.Body).Decode(&users); err != nil {
        return nil, err
    }

    return users, nil
}

// LinkUserToProject links a user to a project by project ID using a POST request to the Semaphore API.
func LinkUserToProject(client *SemaphoreClient, projectID int, linkUserData UserLinkData) error {
    // Define the API endpoint URL for linking a user to a project
    linkUserURL := fmt.Sprintf("%s/project/%d/users", client.baseURL, projectID)

    // Serialize the linkUserData to JSON
    requestData, err := json.Marshal(linkUserData)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for linking the user to the project
    req, err := client.createRequest("POST", linkUserURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to link user to project, status code: %d", resp.StatusCode)
    }

    // The user has been successfully linked to the project

    return nil
}

// RemoveUserFromProject removes a user from a project by project ID and user ID using a DELETE request to the Semaphore API.
func RemoveUserFromProject(client *SemaphoreClient, projectID, userID int) error {
    // Define the API endpoint URL for removing a user from a project
    removeUserURL := fmt.Sprintf("%s/project/%d/users/%d", client.baseURL, projectID, userID)

    // Create an HTTP DELETE request for removing the user from the project
    req, err := client.createRequest("DELETE", removeUserURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove user from project, status code: %d", resp.StatusCode)
    }

    // The user has been successfully removed from the project

    return nil
}

// UserLinkData represents the data for linking a user to a project.
type UserLinkData struct {
    UserID int    `json:"user_id"`
    Role   string `json:"role"`
}
