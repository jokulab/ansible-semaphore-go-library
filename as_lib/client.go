package as_lib

import (
	"bytes"
	"fmt"
	"net/http"
	// Other necessary imports
)

// SemaphoreClient represents a client for interacting with the Semaphore API.
type SemaphoreClient struct {
    baseURL    string
    auth       string
    password   string
    httpClient *http.Client
}

// NewSemaphoreClient creates a new SemaphoreClient.
func NewSemaphoreClient(baseURL, auth, password string) *SemaphoreClient {
    return &SemaphoreClient{
        baseURL: baseURL,
        auth: auth,
        password: password,
        httpClient: &http.Client{},
    }
}

// createRequest creates an HTTP request with the provided method, URL, and request body.
func (c *SemaphoreClient) createRequest(method, url string, requestBody []byte) (*http.Request, error) {
    req, err := http.NewRequest(method, url, bytes.NewBuffer(requestBody))
    if err != nil {
        return nil, err
    }

    // Set headers for the request
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")

    // Set Basic Authentication header
    req.SetBasicAuth(c.auth, c.password)

    return req, nil
}

// doRequest sends an HTTP request and handles the response.
func (c *SemaphoreClient) doRequest(req *http.Request) (*http.Response, error) {
    // Send the HTTP request
    resp, err := c.httpClient.Do(req)
    if err != nil {
        return nil, err
    }

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("request failed, status code: %d", resp.StatusCode)
    }

    return resp, nil
}


// ... Other methods for interacting with the Semaphore API ...