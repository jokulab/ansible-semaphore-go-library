package as_lib

import (
    // "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

// Repository represents a Semaphore repository.
type Repository struct {
    ID         int    `json:"id"`
    Name       string `json:"name"`
    ProjectID  int    `json:"project_id"`
    GitURL     string `json:"git_url"`
    GitBranch  string `json:"git_branch"`
    SSHKeyID   int    `json:"ssh_key_id"`
}

// GetRepositories retrieves repositories for a project by project ID using a GET request to the Semaphore API.
func GetRepositories(client *SemaphoreClient, projectID int, sort, order string) ([]Repository, error) {
    // Define the API endpoint URL for getting repositories for a project
    getRepositoriesURL := fmt.Sprintf("%s/project/%d/repositories?sort=%s&order=%s", client.baseURL, projectID, sort, order)

    // Create an HTTP GET request for getting repositories
    req, err := client.createRequest("GET", getRepositoriesURL, nil)
    if err != nil {
        return nil, err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusOK {
        // Handle non-successful response (e.g., return an error or log)
        return nil, fmt.Errorf("failed to get repositories for project, status code: %d", resp.StatusCode)
    }

    // Read and parse the response body
    var repositories []Repository
    if err := json.NewDecoder(resp.Body).Decode(&repositories); err != nil {
        return nil, err
    }

    return repositories, nil
}

// AddRepository adds a repository to a project by project ID using a POST request to the Semaphore API.
func AddRepository(client *SemaphoreClient, projectID int, newRepository Repository) error {
    // Define the API endpoint URL for adding a repository to a project
    addRepositoryURL := fmt.Sprintf("%s/project/%d/repositories", client.baseURL, projectID)

    // Serialize the newRepository to JSON
    requestData, err := json.Marshal(newRepository)
    if err != nil {
        return err
    }

    // Create an HTTP POST request for adding the repository
    req, err := client.createRequest("POST", addRepositoryURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to add repository, status code: %d", resp.StatusCode)
    }

    // The repository has been successfully added to the project

    return nil
}

// UpdateRepository updates a repository by project ID and repository ID using a PUT request to the Semaphore API.
func UpdateRepository(client *SemaphoreClient, projectID, repositoryID int, updatedRepository Repository) error {
    // Define the API endpoint URL for updating a repository by project ID and repository ID
    updateRepositoryURL := fmt.Sprintf("%s/project/%d/repositories/%d", client.baseURL, projectID, repositoryID)

    // Serialize the updatedRepository to JSON
    requestData, err := json.Marshal(updatedRepository)
    if err != nil {
        return err
    }

    // Create an HTTP PUT request for updating the repository
    req, err := client.createRequest("PUT", updateRepositoryURL, requestData)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to update repository, status code: %d", resp.StatusCode)
    }

    // The repository has been successfully updated

    return nil
}

// RemoveRepository removes a repository by project ID and repository ID using a DELETE request to the Semaphore API.
func RemoveRepository(client *SemaphoreClient, projectID, repositoryID int) error {
    // Define the API endpoint URL for removing a repository by project ID and repository ID
    removeRepositoryURL := fmt.Sprintf("%s/project/%d/repositories/%d", client.baseURL, projectID, repositoryID)

    // Create an HTTP DELETE request for removing the repository
    req, err := client.createRequest("DELETE", removeRepositoryURL, nil)
    if err != nil {
        return err
    }

    // Send the HTTP request
    resp, err := client.doRequest(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Check the response status code
    if resp.StatusCode != http.StatusNoContent {
        // Handle non-successful response (e.g., return an error or log)
        return fmt.Errorf("failed to remove repository, status code: %d", resp.StatusCode)
    }

    // The repository has been successfully removed

    return nil
}
